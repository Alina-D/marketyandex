package helpers;


import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.ArrayList;
import java.util.List;

import static java.lang.String.format;
import static org.junit.Assert.assertEquals;

public class ResultHelper extends CommonHelper{

    private static final Logger logger = LogManager.getLogger(ResultHelper.class);
    private List<Integer> price = new ArrayList<Integer>() {};

    public ResultHelper(ChromeDriver wd) {
        super(wd);
    }

    // ������ "��� �������"
    private static final String ALL_FILTERS_BTN = "a[href*='filters']";
    // ������ �� ��� ������ � ��������� ������
    private static final String PRODUCT_WITH_NAME_LINK = "//li//a[contains(text(), '%s')]";
    // �������� ���� ��������� �������
    private static final String ALL_PRODUCTS_CARD = "div[data-id*='model']";
    // �������� ������� ���������� ������
    private static final String FIRST_PRODUCT_CARD_XPATH = "//div[contains(@data-id, 'model')]/div[3]/div/a";
    // ���� ������
    private static final String SEARCH_FIELD_ID = "header-search";
    // ������ "�����"
    private static final String SELECT_BTN_XPATH = "//span[contains(text(), '�����')]/..";
    // ���������� ������� � ��������� ���������� ����������
    private static final String sortingProductXpath = "//a[contains(text(),'%s')]";
    // ��� ���� �� ������
    private static final String PRICE_PRODUCTS = "a .price";


    /**
     * ������� ��� ������
     * @return this
     */
    public ResultHelper selectProduct(String nameProduct) {
        wd.findElement(By.xpath(format(PRODUCT_WITH_NAME_LINK, nameProduct))).click();
        logger.info(format("������ ��� ������: %s", nameProduct));
        return this;
    }

    /**
     * ������ ������ "��� �������"
     * @return this
     */
    public ResultHelper clickAllFilters() {
        waitVisibilityElement(By.cssSelector(ALL_FILTERS_BTN));
        scrollToElement(By.cssSelector(ALL_FILTERS_BTN));
        wd.findElement(By.cssSelector(ALL_FILTERS_BTN)).click();
        logger.info(format("��������� ������� �� ������ '��� �������'"));
        return this;
    }

    /**
     * ��������� ���������� ��������� ������� �� ������������� � ���������
     * @param countProducts - ��������� ���������� �������
     * @return this
     */
    public ResultHelper checkResultProducts(int countProducts) {
        assertEquals(String.format("���������� ��������� ������� ������ ���� %s", countProducts),
                countProducts, wd.findElements(By.cssSelector(ALL_PRODUCTS_CARD)).size());
        logger.info(format("���������� ��������� ������� �������������: %s ", countProducts));
        return this;
    }

    /**
     * ��������� ����� ������� ������ � ������ �� �����, ��������� �������
     * @return this
     */
    public ResultHelper checkFirstProductAvailability() {
        String nameFirstProduct = wd.findElements(By.xpath(FIRST_PRODUCT_CARD_XPATH)).get(0).getText();
        wd.findElement(By.id(SEARCH_FIELD_ID)).sendKeys(nameFirstProduct);
        wd.findElement(By.xpath(SELECT_BTN_XPATH)).click();
        Assert.assertEquals(format("����� � ������ '%s' �� ������", nameFirstProduct),
                wd.findElements(By.xpath(FIRST_PRODUCT_CARD_XPATH)).get(0).getText(), nameFirstProduct);
        logger.info(format("����� � ������ '%s' ������", nameFirstProduct));
        return this;
    }

    /**
     * ��������� ���������� ������� � ��������� ����������� ���� ����������
     * @param typeSort - ��� ����������
     * @return this
     */
    public ResultHelper sortProduct(String typeSort) {
        wd.findElement(By.xpath(format(sortingProductXpath, typeSort))).click();
        logger.info(format("��������� ���������� ������� '%s'", typeSort));
        return this;
    }

    /**
     * ��������� ���������� ���������� �������
     * @return this
     */
    public ResultHelper checkSortProduct() {
        waitVisibilityElement(By.cssSelector(PRICE_PRODUCTS));
        writePricesOfGoodsToList();
        for (int i = 0; i < price.size(); i++) {
            for (int j = i; j < price.size(); j++) {
                if (i == j) continue;
                Assert.assertTrue("���������� ���������� �� �������!", price.get(i) < price.get(j));
            }
        }
        return this;
    }

    /**
     * �������� � ������ ���� �� ������ �����
     * @return this
     */
    private ResultHelper writePricesOfGoodsToList() {
        wd.findElements(By.cssSelector(PRICE_PRODUCTS)).forEach(element -> {
            String priceProduct = element.getText().substring(0, element.getText().length() - 2)
                    .replaceAll("\\s+", "");
            price.add(Integer.parseInt(priceProduct));
        });
        logger.info("���� ������� ������ ��������� � ������: " + price);
        return this;
    }
}
