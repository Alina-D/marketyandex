package helpers;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static java.lang.String.format;

public class CommonHelper {

    protected ChromeDriver wd;
    public static final Logger logger = LogManager.getLogger(CommonHelper.class);

    public CommonHelper(ChromeDriver wd) {
        this.wd = wd;
    }

    // Ссылка "Маркет"
    private static final String MARKET_LINK = "[data-id='market']";
    // Ссылка "Электоника"
    private static final String ELECTRONIC_LINK = "a[href*='elektronika']";


    /**
     * Нажать на ссылку "Маркет"
     * @return this
     */
    public CommonHelper goInMarket() {
        wd.findElement(By.cssSelector(MARKET_LINK)).click();
        logger.info("Выполено нажатие на ссылку 'Маркет'");
        return this;
    }

    /**
     * Выбрать раздел "Электроника"
     * @return this
     */
    public CommonHelper selectElectronicsSection() {
        wd.findElement(By.cssSelector(ELECTRONIC_LINK)).click();
        logger.info("Выбрать раздел 'Электроника'");
        return this;
    }

    /**
     * Ждать видимости элемента
     * @return this
     */
    protected CommonHelper waitVisibilityElement(By element) {
        (new WebDriverWait(wd, 10))
                .until(ExpectedConditions
                        .visibilityOfElementLocated(element));
        return this;
    }

    /**
     * Выполнить скролл к элементу
     * @return this
     */
    protected CommonHelper scrollToElement(By element) {
        ((JavascriptExecutor)wd).executeScript("arguments[0].scrollIntoView();",
                wd.findElement(element));
        logger.info(format("Выполнен скролл к элементу: %s", element));
        return this;
    }
}
