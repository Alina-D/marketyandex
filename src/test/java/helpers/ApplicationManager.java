package helpers;

import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class ApplicationManager {
    ChromeDriver wd;

    private CommonHelper commonHelper;
    private ResultHelper resultHelper;
    private FilterHelper filterHelper;

    public void init() {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe");
        System.setProperty("selenide.browser", "firefox");
        wd = new ChromeDriver();
        wd.manage().window().maximize();
        wd.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        wd.get("https://yandex.ru/");
        commonHelper = new CommonHelper(wd);
        resultHelper = new ResultHelper(wd);
        filterHelper = new FilterHelper(wd);
    }

    /**
     * ������� �������
     */
    public void stop() {
        wd.quit();
    }

    public CommonHelper commonHelper() {
        return commonHelper;
    }

    public ResultHelper resultHelper() {
        return resultHelper;
    }

    public FilterHelper filterHelper() {
        return filterHelper;
    }
}
