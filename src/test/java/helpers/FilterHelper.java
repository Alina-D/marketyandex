package helpers;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static java.lang.String.format;

public class FilterHelper extends CommonHelper{

    public static final Logger logger = LogManager.getLogger(FilterHelper.class);

    public FilterHelper(ChromeDriver wd) {
        super(wd);
    }

    // ������ "�������� ����������"
    private static final String SHOW_SUITABLE_BTN_XPATH = "//span[contains(text(), '�������� ����������')]/..";
    // ������� ������ ������������� � ��������� ������
    private static final String MANUFACTURER_WITH_NAME_CHECKBOX_XPATH = "//label[contains(text(),'%s')]/..";
    // ���� "���� ��"
    private static final String priceFromField = "glf-pricefrom-var";


    /**
     * ���������� ����� � ���� "���� ��"
     * @param price - �����
     * @return this
     */
    public FilterHelper fillInFieldPriceFrom(String price) {
        wd.findElement(By.name(priceFromField)).sendKeys(price);
        logger.info(format("����������� �����: %s  � ���� '���� ��'", price));
        return this;
    }

    /**
     * ������� �������������
     * @param nameManufacturer - ��� �������������
     * @return this
     */
    public FilterHelper selectManufacturer(String nameManufacturer) {
        wd.findElement(By.xpath(format(MANUFACTURER_WITH_NAME_CHECKBOX_XPATH, nameManufacturer))).click();
        logger.info(format("�������� ����� ���������������: %s", nameManufacturer));
        return this;
    }

    /**
     * ������ ������ "�������� ����������"
     * @return this
     */
    public FilterHelper clickApplyBtn() {
        ((JavascriptExecutor)wd).executeScript("arguments[0].scrollIntoView();",
                wd.findElement(By.xpath(SHOW_SUITABLE_BTN_XPATH)));
        wd.findElement(By.xpath(SHOW_SUITABLE_BTN_XPATH)).click();
        logger.info("��������� ������� ������ '�������� ����������'");
        return this;
    }
}
