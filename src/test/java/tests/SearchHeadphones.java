package tests;

import org.junit.Test;

public class SearchHeadphones extends TestBase {

    @Test
    public void test () {
        app.commonHelper().goInMarket().selectElectronicsSection();
        app.resultHelper().selectProduct("Наушники").clickAllFilters();
        app.filterHelper()
                .fillInFieldPriceFrom("5000")
                .selectManufacturer("Beats")
                .clickApplyBtn();
        app.resultHelper().checkResultProducts(12).checkFirstProductAvailability();
    }
}
