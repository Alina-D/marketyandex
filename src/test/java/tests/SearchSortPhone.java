package tests;

import org.junit.Test;

public class SearchSortPhone extends TestBase {

    @Test
    public void test () {
        app.commonHelper().goInMarket().selectElectronicsSection();
        app.resultHelper()
                .selectProduct("��������� ��������")
                .sortProduct("�� ����")
                .checkSortProduct();
    }
}
