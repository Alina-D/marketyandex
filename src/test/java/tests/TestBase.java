package tests;

import helpers.ApplicationManager;
import org.junit.After;
import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestBase {

    protected final ApplicationManager app = new ApplicationManager();
    Logger logger = LoggerFactory.getLogger(TestBase.class);

    @Before
    public void setUp () {
        app.init();
    }

    @After
    public void tearDown() { app.stop();
    }
}