package tests;

import org.junit.Test;

public class SearchPhone extends TestBase {

    @Test
    public void test () {
        app.commonHelper().goInMarket().selectElectronicsSection();
        app.resultHelper().selectProduct("���������").clickAllFilters();
        app.filterHelper()
                .fillInFieldPriceFrom("20000")
                .selectManufacturer("Samsung")
                .clickApplyBtn();
        app.resultHelper().checkResultProducts(12).checkFirstProductAvailability();
    }


}
